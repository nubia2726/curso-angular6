import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client-model';
import { AppState } from './../../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';
@Component({
  selector: 'app-list-destino',
  templateUrl: './list-destino.component.html',
  styleUrls: ['./list-destino.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates:string[];
  all :any[];

  constructor(private destinoApiclient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state=> state.destinos.favorito)
    .subscribe(d =>{
      if(d != null){
      this.updates.push('se eligio a' + d.nombre);
    }
  });
}


  ngOnInit(){
  }

  agregado (d: DestinoViaje){
    this.destinoApiclient.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido (e: DestinoViaje){
    this.destinoApiclient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

getAll():any{
    return this.all;
  }
}
